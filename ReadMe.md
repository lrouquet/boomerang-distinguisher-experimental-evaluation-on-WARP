# Experimental evaluation of boomerang distinguishers on WARP

This repository provides a tool to experimentally check the probability of a boomerang distinguisher on Warp.
This code is able to compute any boomerang distinguisher with a high enough probability ($`> 2^{-35}`$).

The distinguishers are represented in `json` files, such as:
```json
{
  "nr": 3,
  "input_difference": [ 0, 1, 0, 0, 0, null, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, null, 1, 0, 0, 1, 1  ],
  "output_difference": [ 0, 0, 0, null, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0 ],
  "log2_nb_keys": 4,
  "log2_nb_tries_by_key": 4
}
```

- **nr:** is the number of round of the distinguisher.
- **input_difference:** is the difference injected to generate the pair of plaintext.
- **output_difference:** is the difference injected in the pair of ciphertext.
- **log2_nb_keys:** indicates the number of key to try. $`nb\_keys = 2^{log2\_nb\_keys}`$.
- **log2_nb_tries_by_key:** indicates the number of pairs of plaintext to try for each key. $`nb\_tries\_by\_key = 2^{log2\_nb\_tries\_by\_key}`$

In input and output differences, `null` stands for FREE variables, *i.e.* variables that can take any positive value without changing the probability.
In such case, the software will automatically generate a random value for these variables.

All the distinguishers are computed with the method described in [DBLP:journals/tosc/LallemandMR22](https://tosc.iacr.org/index.php/ToSC/article/view/9716), 
expected for `cryptoeprint-2022-745--9-round.json` and `cryptoeprint-2022-745--14-round.json` which are taken from 
[cryptoeprint:2022/745](https://eprint.iacr.org/2022/745.pdf).

## Usage

The software requires to have [cargo](https://www.rust-lang.org/learn/get-started) installed. To execute the program users can use the following command:
```sh
cargo run --release -- -d distinguishers/3-round.json [-n Number parallel threads]
```
The argument `distinguishers/3-round.json` can be replaced by any other distinguisher file path. 

## Tests

The `WARP` implementation passes the test vectors provided by the cipher authors [cryptoeprint:2020/1320](https://eprint.iacr.org/2020/1320.pdf). To run the tests use:
```sh
cargo test
```

## Version notes

- `1.1.0`: The computation is now done in parallel using the `rayon` crate. By default, the computation will use all the
  threads. To change the number of used threads use the parameter `-n`.
- `1.0.0`: Initial version passing the test vectors

### References
@article{DBLP:journals/tosc/LallemandMR22,
author    = {Virginie Lallemand and
Marine Minier and
Lo{"{\i}}c Rouquette},
title     = {Automatic Search of Rectangle Attacks on Feistel Ciphers: Application
to {WARP}},
journal   = {{IACR} Trans. Symmetric Cryptol.},
volume    = {2022},
number    = {2},
pages     = {113--140},
year      = {2022}
}

@misc{cryptoeprint:2022/745,
author = {Hosein Hadipour and Marcel Nageler and Maria Eichlseder},
title = {Throwing Boomerangs into Feistel Structures: Application to CLEFIA, WARP, LBlock, LBlock-s and TWINE},
howpublished = {Cryptology ePrint Archive, Paper 2022/745},
year = {2022},
note = {\url{https://eprint.iacr.org/2022/745}},
url = {https://eprint.iacr.org/2022/745}
}

@misc{cryptoeprint:2020/1320,
author = {Subhadeep Banik and Zhenzhen Bao and Takanori Isobe and Hiroyasu Kubo and Fukang Liu and Kazuhiko Minematsu and Kosei Sakamoto and Nao Shibata and Maki Shigeri},
title = {WARP : Revisiting GFN for Lightweight 128-bit Block Cipher},
howpublished = {Cryptology ePrint Archive, Paper 2020/1320},
year = {2020},
note = {\url{https://eprint.iacr.org/2020/1320}},
url = {https://eprint.iacr.org/2020/1320}
}